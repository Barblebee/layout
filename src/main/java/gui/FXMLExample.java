package gui;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FXMLExample extends Application {

    private static final String MAZE_PATH = System.getProperty("user.dir") + "/Mazes/";

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        Problem mazeOld = Problem.load(MAZE_PATH + "example1");
        ProblemCanvas problemCanvas = new ProblemCanvas(mazeOld, 5);
        Insets insets = new Insets(5, 5 ,5, 5);

        // ScrollPane  -----------------------
        ScrollPane scrollPane = new ScrollPane(problemCanvas);

        // Slider
        Slider slider = new Slider(2,20,20);
        slider.setMajorTickUnit(10);
        slider.setBlockIncrement(10);
        slider.setShowTickMarks(true);
        problemCanvas.cellWidthProperty().bind(slider.valueProperty());

        // HBox for comboBox and slider
        HBox hBoxSlider = new HBox();
        ComboBox comboBox = new ComboBox(FXCollections.observableArrayList("Set wall", "Set start", "Set goal"));
        problemCanvas.drawModeProperty().bind(comboBox.valueProperty());
        comboBox.setValue("Set wall");
        comboBox.setVisibleRowCount(4);
        HBox.setHgrow(slider, Priority.ALWAYS);
        hBoxSlider.setPadding(insets);
        hBoxSlider.getChildren().addAll(comboBox, slider);

        // Grid pane: scrollPane with slider
        GridPane gridPane = new GridPane();
//        gridPane.setGridLinesVisible(true);

        RowConstraints row = new RowConstraints();
        row.setVgrow(Priority.ALWAYS);
        gridPane.getRowConstraints().add(row);
        ColumnConstraints col = new ColumnConstraints();
        col.setHgrow(Priority.ALWAYS);
        gridPane.getColumnConstraints().add(col);
        gridPane.addRow(0, scrollPane);
        gridPane.addRow(1, hBoxSlider);

        // TabPane -----------------
        TabPane tabPane = new TabPane();

        // ListView (File)
        String directory = MAZE_PATH;
        System.out.println(directory);
        List<String> fileNames = new ArrayList<>();
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(directory))) {
            for (Path path : directoryStream) {
                if (!Files.isDirectory(path)) fileNames.add(path.getFileName().toString());
            }
        } catch (IOException ex) {}
        Collections.sort(fileNames);

        ObservableList<String> names = FXCollections.observableArrayList(fileNames);
        ListView<String> listViewFile = new ListView<>(names);
        listViewFile.getSelectionModel().selectedItemProperty().addListener (new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldFilename, String newFilename) {
                System.out.println("Change");
                Problem newMazeOld = Problem.load(MAZE_PATH + newFilename);
                problemCanvas.setProblem(newMazeOld);
            }
        });

        // HBox with buttons for file access
        HBox hBoxFile = new HBox();
        hBoxFile.setPadding(insets);
        hBoxFile.setSpacing(5);

        List<Button> fileButtonList = new ArrayList<>();
        fileButtonList.add(new Button("Save"));
        fileButtonList.add(new Button("Save As"));
        fileButtonList.add(new Button("Create"));
        fileButtonList.add(new Button("Delete"));
        fileButtonList.forEach(button -> button.setPrefWidth(70));
        hBoxFile.getChildren().addAll(fileButtonList);

        // VBox with ListView and buttons for file access
        VBox vBoxFile = new VBox();
        VBox.setVgrow(listViewFile, Priority.ALWAYS);
        vBoxFile.getChildren().addAll(listViewFile, hBoxFile);


        Tab tabFile = new Tab("File");
        tabFile.setContent(vBoxFile);
        tabPane.getTabs().add(tabFile);

        HBox sceneHBox = new HBox();
        HBox.setHgrow(gridPane, Priority.ALWAYS);
        tabPane.setMinWidth(320);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        sceneHBox.getChildren().addAll(tabPane, gridPane);

        sceneHBox.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
//            System.out.println(hBoxFile.getPrefWidth());
//            System.out.println(listViewFile.getPrefHeight());
        });

        Scene scene = new Scene(sceneHBox, 800, 600);
        stage.setTitle("GridPaneTest");
        Tooltip sceneTip = new Tooltip("Left/Right MouseButton -> Set/Erase");
        comboBox.setTooltip(sceneTip);
        stage.setScene(scene);
        stage.show();
    }
}

