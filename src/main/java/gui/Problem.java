package gui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Problem extends Layout {

    private Point start;
    private Point goal;

    public static Problem load (String fileName) {
        Layout layout = Layout.load(fileName);
        Point start = null;
        Point goal = null;
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                switch (line) {
                    case "-- start --":
                        int startX = Integer.parseInt(br.readLine());
                        int startY = Integer.parseInt(br.readLine());
                        start = new Point(startX , startY);
                        break;
                    case "-- goal --":
                        int goalX = Integer.parseInt(br.readLine());
                        int goalY = Integer.parseInt(br.readLine());
                        goal = new Point(goalX, goalY);
                        break;
                }
            }
        } catch (IOException e) { e.printStackTrace(); }

        Problem newProblem = new Problem(layout, start, goal);

        return newProblem;
    }

    public Problem(Layout layout, Point start, Point goal) {
        super (layout);
        this.start = start;
        this.goal = goal;
    }

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Point getGoal() {
        return goal;
    }

    public void setGoal(Point goal) {
        this.goal = goal;
    }

    @Override
    public void printNicely() {
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                if (x == start.getX() && y == start.getY()) {
                    System.out.print("S");
                    continue;
                }
                if (x == goal.getX() && y == goal.getY()) {
                    System.out.print("G");
                    continue;
                }
                switch (getCellType(x, y)) {
                    case WALL:
                        System.out.print("#");
                        break;
                    case EMPTY:
                        System.out.print("_");
                        break;
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}