package gui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Layout {

    public static enum CellType { EMPTY, WALL }
    private CellType[][] cells = null;

    public static Layout load(String fileName) {
        List<String> stringRepresentation = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String readLine;
            while ((readLine = br.readLine()) != null) {
                if (readLine.equals("-- start --")) break;
                else stringRepresentation.add(readLine);
            }
        }
        catch (IOException e) { e.printStackTrace(); }

        int width = stringRepresentation.get(0).length();
        int height = stringRepresentation.size();

        Layout layout = new Layout(width, height);
        int y = 0;
        for (String line : stringRepresentation) {
            char[] chars = line.toCharArray();
            int x = 0;
            for (char c : chars) {
                switch (c) {
                    case '0':
                        layout.setCellType(x, y, CellType.EMPTY);
                        break;
                    case '1':
                        layout.setCellType(x, y, CellType.WALL);
                        break;
                }
                x++;
            }
            y++;
        }
        return layout;
    }

    public Layout(int width, int height) {
        cells = new CellType[width][height];
        for (CellType[] column : cells) {
            Arrays.fill(column, CellType.EMPTY);
        }
    }

    public Layout(Layout layout) {
        this.cells = layout.cells;
    }

    public CellType getCellType(int x, int y) {
        return cells[x][y];
    }

    public void setCellType(int x, int y, CellType newCellType) {
        cells[x][y] = newCellType;
    }

    public int getWidth() {
        return cells.length;
    }

    public void setWidth(int width) {
        resize(width, getHeight());
    }

    public int getHeight() {
        return cells[0].length;
    }

    public void setHeight(int height) {
            resize(getWidth(), height);
    }

    public void resize(int width, int height) {
        int oldWidth = getWidth();
        int oldHeight = getHeight();
        CellType newCells[][] = new CellType[width][height];
        for(int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (x < oldWidth && y < oldHeight)
                    newCells[x][y] = cells[x][y];
                else
                    newCells[x][y] = CellType.EMPTY;
            }
        }
        this.cells = newCells;
    }

    public void printNicely() {
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                switch (getCellType(x, y)) {
                    case WALL:
                        System.out.print("#");
                        break;
                    case EMPTY:
                        System.out.print("_");
                        break;
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}