package gui;

import javafx.beans.property.*;

import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.*;
import javafx.scene.paint.Color;

public class ProblemCanvas extends Canvas {

    private Problem problem;
    private StringProperty drawMode;
    private IntegerProperty cellWidth;
    private GraphicsContext gc;

    ProblemCanvas(Problem problem, int cellWidth) {
        this.problem = problem;
        this.cellWidth = new SimpleIntegerProperty(cellWidth);
        this.drawMode = new SimpleStringProperty("Set Wall");
        int width = problem.getWidth() * getCellWidth();
        int height = problem.getHeight() * getCellWidth();
        this.setWidth(width);
        this.setHeight(height);
        this.gc = getGraphicsContext2D();
        update();

        addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            int x = (int) (event.getX() / cellWidthProperty().get());
            int y = (int) (event.getY() / cellWidthProperty().get());
            if (x > problem.getWidth()-1) return;
            if (y > problem.getHeight()-1) return;
            switch (getDrawMode()) {
                case "Set wall":
                    if (event.isPrimaryButtonDown()) problem.setCellType(x,y, Problem.CellType.WALL);
                    else problem.setCellType(x,y, Problem.CellType.EMPTY);
                    update();
                    break;
                case "Set start":
                    problem.setStart(new Point(x, y));
                    update();
                    break;
                case "Set goal":
                    problem.setGoal(new Point(x, y));
                    update();
                    break;
            }
            event.consume();
        });

        addEventHandler(MouseEvent.MOUSE_MOVED, event -> setCursor(Cursor.CROSSHAIR));
        cellWidthProperty().addListener((observable, oldValue, newValue) -> update());
        drawModeProperty().addListener((observable, oldValue, newValue) -> update());
    }

    void update() {
        int cellWidth = this.cellWidth.intValue();
        setWidth(problem.getWidth() * cellWidth);
        setHeight(problem.getHeight() * cellWidth);
        gc.clearRect(0,0, getWidth(), getHeight());
        gc.setLineWidth(0.1);
        for (int x = 0; x < problem.getWidth(); x++) {
            for (int y = 0; y < problem.getHeight(); y++) {
                Problem.CellType cellType = problem.getCellType(x, y);
                switch (cellType) {
                    case EMPTY:
                        gc.setFill(Color.TRANSPARENT);
                        gc.strokeRect(x * cellWidth, y * cellWidth, cellWidth, cellWidth);
                        break;
                    case WALL:
                        gc.setFill(Color.BLACK);
                        gc.fillRect(x * cellWidth, y * cellWidth, cellWidth, cellWidth);
                        break;

                }
            }
        }

        Point start = problem.getStart();
        if (start != null) {
            if (start.isInRegion(problem.getWidth(), problem.getHeight())) {
                int x = start.getX() * cellWidth;
                int y = start.getY() * cellWidth;
                gc.setFill(Color.GREEN);
                gc.fillRect(x, y, cellWidth, cellWidth);
            }
        }

        Point goal = problem.getGoal();
        if (goal != null) {
            if (goal.isInRegion(problem.getWidth(), problem.getHeight())) {
                int x = goal.getX() * cellWidth;
                int y = goal.getY() * cellWidth;
                gc.setFill(Color.RED);
                gc.fillRect(x, y, cellWidth, cellWidth);
            }
        }

        return;
    }


    void setCellType(int x, int y, Problem.CellType cellType) {
        problem.setCellType(x, y, cellType);
        update();
    }

    Problem.CellType getCellType(int x, int y) {
        return problem.getCellType(x,y);
    }

    void setProblem(Problem problem) {
        this.problem = problem;
        update();
    }

    Problem getProblem() {
        return problem;
    }

    public final int getCellWidth() {
        return cellWidth.intValue();
    }

    public final void setCellWidth(int width) {
        this.cellWidthProperty().set(width);
        update();
    }

    public IntegerProperty cellWidthProperty() {
        return cellWidth;
    }

    public final String getDrawMode() {
        return drawMode.get();
    }

    public final void setDrawMode(String drawMode) {
        this.drawMode.setValue(drawMode);
    }

    public final StringProperty drawModeProperty() {
        return drawMode;
    }
}
