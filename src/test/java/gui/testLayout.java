package gui;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class testLayout {

    @Test
    public void testLoad() {
        // Load 2x2-layout
        Layout layout = Layout.load(System.getProperty("user.dir") + "/build/resources/test/testLayout2x2");
        layout.printNicely();

        // check width and height
        assertEquals(2, layout.getWidth());
        assertEquals(2, layout.getHeight());

        // check all cell types
        assertSame(Layout.CellType.EMPTY, layout.getCellType(0,0));
        assertSame(Layout.CellType.WALL, layout.getCellType(0,1));
        assertSame(Layout.CellType.WALL, layout.getCellType(1,0));
        assertSame(Layout.CellType.EMPTY, layout.getCellType(1,1));
    }

    // Resize tests
    @Test
    public void testResize1() {
        Layout layout = Layout.load(System.getProperty("user.dir") + "/build/resources/test/testLayout2x2");
        layout.printNicely();
        layout.setWidth(3);
        layout.setHeight(3);
        layout.printNicely();
        assertEquals(layout.getWidth(), 3);
        assertEquals(layout.getHeight(), 3);
        assertSame(layout.getCellType(0, 0), Layout.CellType.EMPTY);
        assertSame(layout.getCellType(0, 1), Layout.CellType.WALL);
        assertSame(layout.getCellType(0, 2), Layout.CellType.EMPTY);
        assertSame(layout.getCellType(1, 0), Layout.CellType.WALL);
        assertSame(layout.getCellType(1, 1), Layout.CellType.EMPTY);
        assertSame(layout.getCellType(1, 2), Layout.CellType.EMPTY);
        assertSame(layout.getCellType(2, 2), Layout.CellType.EMPTY);
        assertSame(layout.getCellType(2, 1), Layout.CellType.EMPTY);
        assertSame(layout.getCellType(2, 0), Layout.CellType.EMPTY);
    }

    @Test
    // Resize 3x3->2x2 and check width, height, cellTypes,
    // if start and goal set to null (because out of new layout dimensions)
    // if exception is thrown in case of x, y out of bounds
    public void testResize2() {
        Layout layout = Layout.load(System.getProperty("user.dir") + "/build/resources/test/testLayout3x3");

        layout.setWidth(2);
        layout.setHeight(2);
        assertEquals(layout.getWidth(), 2);
        assertEquals(layout.getHeight(), 2);

        assertSame(layout.getCellType(0,0), Layout.CellType.WALL);
        assertSame(layout.getCellType(0,1), Layout.CellType.EMPTY);
        assertSame(layout.getCellType(1,0), Layout.CellType.EMPTY);
        assertSame(layout.getCellType(1,1), Layout.CellType.WALL);

        try {
            layout.getCellType(2,2);
            fail("Should throw ArrayIndexOutOfBoundsException!");
        } catch (ArrayIndexOutOfBoundsException e) {}
    }
    
}
