package gui;

import org.junit.Test;
import static org.junit.Assert.*;

public class testProblem {

    @Test
    public void testLoad() {
        Problem problem = Problem.load(System.getProperty("user.dir") + "/build/resources/test/testLayout2x2");
        problem.printNicely();

        assertEquals(problem.getWidth(), 2);
        assertEquals(problem.getHeight(), 2);

        assertSame(problem.getCellType(0,0), Layout.CellType.EMPTY);
        assertSame(problem.getCellType(0,1), Layout.CellType.WALL);
        assertSame(problem.getCellType(1,0), Layout.CellType.WALL);
        assertSame(problem.getCellType(1,1), Layout.CellType.EMPTY);

        assertEquals(problem.getStart(), new Point(0, 0));
        assertEquals(problem.getGoal(), new Point(1, 1));
    }
}
